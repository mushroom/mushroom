<?php

use Whoops\Run;
use Dotenv\Dotenv;
use App\RequestHandler;
use Mushroom\Container;
use Mushroom\Application;
use Mushroom\Contracts\Handler;
use Whoops\Handler\PrettyPageHandler;

require_once __DIR__.'/../vendor/autoload.php';

Dotenv::createMutable(__DIR__ . '/..')->load();

$whoops = new Run();
$whoops->pushHandler(new PrettyPageHandler());
$whoops->register();

Container::bind(Application::class, fn ($container) => new Application($container));
return Container::resolve(Application::class);
