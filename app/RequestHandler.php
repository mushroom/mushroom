<?php

namespace App;

use App\Processors\ExampleProcessor;
use Mushroom\RequestHandler as BaseHandler;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class RequestHandler extends BaseHandler
{
    protected array $processors = [
        ExampleProcessor::class,
    ];
    
    public function handle(Request $request): Response
    {
        return parent::handle($request);
    }

    protected function getResponse(Request $request): Response
    {
        return parent::getResponse($request);
    }
}