<?php

namespace App\Processors;

use Closure;
use Mushroom\Contracts\Processor;
use Symfony\Component\HttpFoundation\Response;

class ExampleProcessor implements Processor
{
    public function process(Response $response, Closure $next): Response
    {
        return $next($response);
    }
}
