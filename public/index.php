<?php

use GuzzleHttp\Client;
use Mushroom\Pipeline;
use App\RequestHandler;
use Mushroom\Container;
use GuzzleHttp\ClientInterface;
use Mushroom\Contracts\Handler;
use Mushroom\Contracts\Pipeline as PipelineInterface;

$app = require_once __DIR__ . '/../bootstrap/app.php';

Container::bind(Handler::class, fn ($container) => $container->get(RequestHandler::class));
// Container::bind(PipelineInterface::class, fn ($container) => $container->get(Pipeliene::class));
// Container::bind(ClientInterface::class, fn ($container) => $container->get(Client::class));

$app->run();
